import mongoose from 'mongoose';
let DASHBOARD_CHART_DATA = new mongoose.Schema({

}, { strict: false, timestamps: true });

let SEGMENT_SETTING = new mongoose.Schema({

}, { strict: false, timestamps: true });
let SEGMENT_MAPPINGS = new mongoose.Schema({

}, { strict: false, timestamps: true });
let PROJECT = new mongoose.Schema({

}, { strict: false, timestamps: true });

let MONITORING_VALUES = new mongoose.Schema({

}, { strict: false, timestamps: true });

let NOTIFICATIONS = new mongoose.Schema({

}, { strict: false, timestamps: true });



mongoose.model('segment_setting', SEGMENT_SETTING);
mongoose.model('dashboard_chart_data', DASHBOARD_CHART_DATA);
mongoose.model('project', PROJECT);
mongoose.model('monitoring_value', MONITORING_VALUES);
mongoose.model('segment_mapping', SEGMENT_MAPPINGS);
mongoose.model('notification', NOTIFICATIONS);