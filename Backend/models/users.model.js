import { AppSetting } from '../config/app.setting';
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const CONFIG = AppSetting.getConfig();
import mongoose from 'mongoose';
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        required: false,
        default: true
    },
    companyAdmin: {
        type: Boolean,
        required: false,
        default: false
    },
    supportUser: {
        type: Boolean,
        required: false,
        default: false
    },
    hash: String,
    salt: String,
    projectID: {
        type: String,
        required: false
    },
    jobTitle: {
        type: String,
        required: false
    },
    assigned_projects: {
        type : Array,
        required: false
    },
    selfie: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: false
    },
    notes: {
        type: String,
        required: false
    },
    birthday: {
        type: Date,
        required: false
    },
    superAdmin: {
        type: Boolean,
        required: false
    },

    phone: {
        type: String,
        require: true
    },
    notification_settings: mongoose.Schema.Types.Mixed
}, { strict: false });
userSchema.methods.setPassword = function (password) {

    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};
userSchema.methods.validatePassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

userSchema.methods.generateJwt = function (timeExp) {
    var expiry = new Date();
    var expiryDays = timeExp || 1;
    console.log(expiryDays)
    expiry.setDate(expiry.getDate() + expiryDays);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        isSuperAdmin: this.superAdmin ? true : false,
        companyAdmin: this.companyAdmin,
        projectID: this.projectID,
        assigned_projects: this.assigned_projects,
        supportUser: this.supportUser,
        exp: parseInt(expiry.getTime() / 1000),
    }, CONFIG.APP.PASSWORD_SECRET); // DO NOT KEEP YOUR SECRET IN THE CODE!
};
userSchema.methods.toAuthJSON = function () {
    return {
        _id: this._id,
        email: this.email,
        projectID: this.projectID,
        name: this.name,
        selfie: this.selfie,
        superAdmin: this.superAdmin,
        assigned_projects: this.assigned_projects,
        companyAdmin: this.companyAdmin,
        supportUser: this.supportUser,
        token: this.generateJwt(),
    };
};

mongoose.model('user', userSchema);
