var forSuperAdminOnly = function (req, res, next) {
    if (req.userInfo.isSuperAdmin) {
        next()
    } else {
        next('Permission denied');
    }
}


var forAdminsOnly = function (req, res, next) {
    if (req.userInfo.isSuperAdmin || req.userInfo.isVendor || req.userInfo.companyAdmin) {
        next()
    } else {
        next('Permission denied');
    }
}

module.exports = {
    forSuperAdminOnly: forSuperAdminOnly,
    forAdminsOnly: forAdminsOnly
}