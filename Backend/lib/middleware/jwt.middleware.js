import passport from 'passport';
import ProjectService from '../../app/dashboard/services/project-settings.service';
import UserManagementService from '../../app/dashboard/services/user-management.service';
import Api from '../api';
function jwt(req, res, next) {
    passport.authenticate('local', { session: false }, async (err, passportUser, info) => {
        if (err) {
            Api.serverError(req, res, 'Login Failed');
        }

        if (passportUser) {
            const user = passportUser;
            const formattedData = await formatUserAndProject(user);
            return res.json(formattedData);
        }

        Api.serverError(req, res, 'Login Failed');
    })(req, res, next);
}
async function formatUserAndProject(user) {
    let formattedUser = user.toAuthJSON();
    let project = await ProjectService.getProjectDetailsByID(user.projectID);
    if (project) {
        project = project.toObject();
        formattedUser.zipCode = project.zipCode;
        project.supportUserDetails = await UserManagementService.getUserDataByID(project.support_user);

    }
    return { user: formattedUser, projectDetails: project };


}
module.exports = jwt;
