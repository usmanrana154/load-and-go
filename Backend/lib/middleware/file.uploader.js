import multer from 'multer';
const PATH = './uploads';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
let fileNameSaved = null;

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, `${PATH}/orignal`);
    },
    filename: (req, file, cb) => {
        var datetimestamp = Date.now();
        fileNameSaved = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];

        req.fileNameSaved = fileNameSaved;
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
const resizeImages = async (req, res, next) => {

    const { filename: image } = req.file;
    await sharp(path.resolve(req.file.destination, image))
        .resize(500)
        .jpeg({ quality: 90 })
        .toFile(
            path.resolve(PATH, image)
        )
    //fs.unlinkSync(req.file.path);
    req.body.image = req.file.filename;
    next();
}
let upload = multer({
    storage: storage
});

module.exports = {
    upload: upload,
    resizeImages: resizeImages
};