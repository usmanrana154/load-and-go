import SegmentSettingsController from '../controllers/segment-settings.controller';
import auth from '../../../lib/middleware/auth.middleware';
import access from '../../../lib/middleware/access.middleware';
module.exports = async (router) => {
	router.get('/settings/segment-settings/:projectID', auth.required,  await SegmentSettingsController.getSegmentSettings);
	router.post('/settings/segment-settings', auth.required, access.forAdminsOnly, await SegmentSettingsController.saveSegmentSettings);
	router.put('/settings/segment-settings', auth.required, access.forAdminsOnly, await SegmentSettingsController.updateSegmentSettings);
	router.post('/settings/initialize-segment-settings', auth.required, access.forAdminsOnly, await SegmentSettingsController.initializeSegSettings);
	router.get('/settings/segment-mappings/:projectID', auth.required, await SegmentSettingsController.getSegmentMappings);
	router.post('/settings/segment-mappings', auth.required, access.forAdminsOnly, await SegmentSettingsController.saveSegmentMappings);
};