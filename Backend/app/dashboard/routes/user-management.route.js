import UserManagementController from '../controllers/user-management.controller';
import auth from '../../../lib/middleware/auth.middleware'
import jwtM from '../../../lib/middleware/jwt.middleware'
import access from '../../../lib/middleware/access.middleware'
import FileUploader from '../../../lib/middleware/file.uploader';
module.exports = async (router) => {
    router.get('/users', auth.required, access.forAdminsOnly, await UserManagementController.getUsers);
    router.get('/users/supportUsers', auth.required, access.forAdminsOnly, await UserManagementController.getSupportUsers);
    router.post('/users', auth.required, access.forAdminsOnly, await UserManagementController.createUser);
    router.post('/login', jwtM);
    router.post('/uploadProfile/:id', auth.required, access.forAdminsOnly, FileUploader.upload.single('file'), FileUploader.resizeImages, await UserManagementController.uploadProfilePicture);
    router.put('/users/:id', auth.required, access.forAdminsOnly, await UserManagementController.updateUser);
    router.delete('/users/:id', auth.required, access.forAdminsOnly, await UserManagementController.deleteUser);
};