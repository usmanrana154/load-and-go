import dashboardController from '../controllers/dashboard.controller';
import auth from '../../../lib/middleware/auth.middleware';
import access from '../../../lib/middleware/access.middleware';
module.exports = async (router) => {
	router.get('/dashboard/data/byCycleID/:cycleID?', auth.required, await dashboardController.getData);
	router.get('/dashboard/data/byProjectID/:projectID?', auth.required, await dashboardController.getDataByProjectId);
	router.post('/dashboard/data/avgValuesCycles', auth.required, await dashboardController.avgValuesCycles);
	router.post('/dashboard/data/loadCycles', auth.required, await dashboardController.loadCycles);
	router.post('/dashboard/data/wHPostData', await dashboardController.webHookCtrl)



};