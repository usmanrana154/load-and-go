import MonitoringController from '../controllers/monitoring.controller';
import auth from '../../../lib/middleware/auth.middleware';
import access from '../../../lib/middleware/access.middleware';

module.exports = async (router) => {
    router.get('/monitoring/:projectID', auth.required, await MonitoringController.getMonitoringSettings);
    router.post('/monitoring/initialize', auth.required, access.forSuperAdminOnly, await MonitoringController.initializeMonSettings);
    router.put('/monitoring', auth.required, MonitoringController.updateMonitoringSettings)
};