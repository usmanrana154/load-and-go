import ProjectSettingsController from '../controllers/project-settings.controller';
import auth from '../../../lib/middleware/auth.middleware';
import access from '../../../lib/middleware/access.middleware';
import FileUploader from '../../../lib/middleware/file.uploader';


module.exports = async (router) => {
    router.get('/settings/projects/', auth.required, await ProjectSettingsController.getProjectsList);
    router.get('/settings/projectDetails/:id', auth.required, await ProjectSettingsController.getDeepdetailsProject);
    router.get('/settings/projects/:id', auth.required, await ProjectSettingsController.getProjectDetailsByID);
    router.post('/settings/projects', auth.required, access.forAdminsOnly, await ProjectSettingsController.saveProject);
    router.post('/settings/projects/uploadImages/:id', auth.required, access.forAdminsOnly, FileUploader.upload.single('file'), FileUploader.resizeImages, await ProjectSettingsController.uploadImages);
    router.put('/settings/projects/:id', auth.required, access.forAdminsOnly, await ProjectSettingsController.updateProject);
    router.delete('/settings/projects/:id', auth.required, access.forAdminsOnly, await ProjectSettingsController.deleteProject);
};