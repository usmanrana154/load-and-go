import ReportController from '../controllers/report.controller';
import auth from '../../../lib/middleware/auth.middleware';
import access from '../../../lib/middleware/access.middleware';

module.exports = async (router) => {
    router.post('/reports/getReport', auth.required, await ReportController.prepareReportData);
    router.post('/reports/getDetailReport', auth.required, await ReportController.prepareDetailReportData);

};