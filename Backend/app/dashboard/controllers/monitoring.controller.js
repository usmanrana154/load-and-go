import MonitoringService from '../services/monitoring.service';
import Api from '../../../lib/api'
class DashboardController {

    constructor() { }

    async getMonitoringSettings(request, response) {
        try {
            let result = await MonitoringService.getMonitoringSettings(request.params.projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async saveMonitoringSettings(request, response) {
        try {
            let result = await MonitoringService.saveMonitoringSettings(request.body.projectID, request.body.cycleID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async updateMonitoringSettings(request, response) {
        try {
            let result = await MonitoringService.updateMonitoringSettings(request.body._id, request.body.settings);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async initializeMonSettings(request, response) {
        try {
            let result = await MonitoringService.initiallizeMonitoring(request.body.projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }






}
export default new DashboardController();