import UserManagementService from '../services/user-management.service';
import Api from '../../../lib/api'
class UserManagementController {

    constructor() { }

    async getUsers(request, response) {
        try {
            let result = await UserManagementService.getUsers(request);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async getSupportUsers(request, response) {
        try {
            let result = await UserManagementService.getSupportUsers();
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async createUser(request, response) {
        try {
            let { user } = request.body;
            let result = await UserManagementService.createUser(user);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async updateUser(request, response) {
        try {
            let { user } = request.body;
            let result = await UserManagementService.updateUser(user, request.params.id);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async deleteUser(request, response) {
        try {
            let result = await UserManagementService.deleteUser(request.params.id);
            Api.ok(request, response, result);
        } catch (e) {
            Api.serverError(request, response, err);
        }
    }
    async uploadProfilePicture(request, response) {
        try {
            let result = await UserManagementService.uploadProfilePicture(request.params.id, request.fileNameSaved);
            Api.ok(request, response, result);
        } catch (e) {
            Api.serverError(request, response, e);
        }
    }






}
export default new UserManagementController();