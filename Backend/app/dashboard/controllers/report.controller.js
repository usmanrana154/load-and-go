import ReportService from '../services/report.service';
import Api from '../../../lib/api'
class ReportController {

    constructor() { }

    async prepareReportData(request, response) {
        try {
            let start = request.body.startDate,
                end = request.body.endDate,
                projectID = request.body.projectID
            let result = await ReportService.prepareReprtData(start, end, projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async prepareDetailReportData(request, response) {
        try {
            let start = request.body.startDate,
                end = request.body.endDate,
                projectID = request.body.projectID
            let result = await ReportService.prepareDetailReportData(start, end, projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

}
export default new ReportController();