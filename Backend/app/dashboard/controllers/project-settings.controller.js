import ProjectSettingsService from '../services/project-settings.service';
import Api from '../../../lib/api'
class ProjectSettingsController {

    constructor() { }

    async getProjectsList(request, response) {
        try {
            let result = await ProjectSettingsService.getProjectsList(request);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async getProjectDetailsByID(request, response) {
        try {
            let result = await ProjectSettingsService.getProjectDetailsByID(request.params.id);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async getDeepdetailsProject(request, response) {
        try {
            let result = await ProjectSettingsService.getDeepdetailsProject(request.params.id);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async saveProject(request, response) {
        try {
            let { project } = request.body;
            let result = await ProjectSettingsService.saveProject(project);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async updateProject(request, response) {
        try {
            let { project } = request.body;
            let result = await ProjectSettingsService.updateProject(project, request.params.id);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async uploadImages(request, response) {
        try {
            let result = await ProjectSettingsService.uploadImages(request.params.id, request.body.image, request.body.type);
            Api.ok(request, response, result);
        } catch (e) {
            Api.serverError(request, response, e);
        }
    }

    async deleteProject(request, response) {
        try {
            let result = await ProjectSettingsService.deleteProject(request.params.id);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }






}
export default new ProjectSettingsController();