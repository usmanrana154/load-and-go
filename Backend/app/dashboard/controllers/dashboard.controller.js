import dashboardService from '../services/dashboard.service';
import Api from '../../../lib/api'
class DashboardController {

    constructor() { }

    async getData(request, response) {
        try {
            let result = await dashboardService.getData(request.params.cycleID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async getDataByProjectId(request, response) {
        try {
            let result = await dashboardService.getDataByProjectId(request.params.projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async loadCycles(request, response) {
        try {
            let result = await dashboardService.loadCycles(request);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async avgValuesCycles(request, response) {
        try {
            let result = await dashboardService.avgValuesCycles(request);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async webHookCtrl(request, response) {
        try {
            let result = await dashboardService.webHookCtrl(request, request.body.cycleID, request.body.projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }






}
export default new DashboardController();