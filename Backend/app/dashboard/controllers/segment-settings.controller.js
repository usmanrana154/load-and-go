import SegmentSettingsService from '../services/segment-settings.service';
import Api from '../../../lib/api'
class DashboardController {

    constructor() { }

    async getSegmentSettings(request, response) {
        try {
            let result = await SegmentSettingsService.getSegmentSettings(request.params.projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async saveSegmentSettings(request, response) {
        try {
            let result = await SegmentSettingsService.saveSegmentSettings(request.body.projectID, request.body.forceFully);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async updateSegmentSettings(request, response) {
        try {
            let result = await SegmentSettingsService.updateSegmentSettings(request.body);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }

    async initializeSegSettings(request, response) {
        try {
            let projectID = request.body.projectID;
            let dateInit = new Date();
            let result = await SegmentSettingsService.initializeSegSettings(projectID, dateInit);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async getSegmentMappings(request, response) {
        try {
            let result = await SegmentSettingsService.getSegmentMappings(request.params.projectID);
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }
    async saveSegmentMappings(request, response) {
        try {
            let mappings = request.body.mappings.map(m => {
                return ({ key: m.key, mapping: m.mapping })
            });
            let result = await SegmentSettingsService.saveSegmentMappings({ projectID: request.body.projectID, mappings: mappings });
            Api.ok(request, response, result);
        } catch (err) {
            Api.serverError(request, response, err);
        }
    }






}
export default new DashboardController();