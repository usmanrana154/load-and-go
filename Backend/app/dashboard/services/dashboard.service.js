import SegmentSettingsService from './segment-settings.service';
import MonitoringSettingsService from './monitoring.service';
import AlertsSender from '../../../utilities/helpers/sendAlerts';
import NOTIFICATION_REPOSITORY from './notification.service';

import mongoose from 'mongoose';
import _ from 'lodash';
const dashboard_chart_data = mongoose.model('dashboard_chart_data');
class DashboardService {
  constructor() { }
  async getData(cycleID) {
    if (!cycleID || cycleID == 'undefined') {
      cycleID = "1fcdce68-5e98-4741-aad3-e3573654b8c3";
    }
    let cycles = await dashboard_chart_data.find({ "cycleID": cycleID }).lean();
    let alerts = await NOTIFICATION_REPOSITORY.getNotificationByCycleID(cycleID);
    let segments = this.formatSegmentsOfCycleData(cycles);
    return { cycle: cycles, segments: segments, alerts: alerts };

  }

  async getDataByProjectId(projectID) {

    //let response = await dashboard_chart_data.find({ "cycleID": cycleID });
    return [];

  }
  formatSegmentsOfCycleData(data) {
    let array1 = data.map((m) => {
      return ({ key: "" + m.M1 + m.M2 + m.M3 + m.M4 + m.M5 + m.M6, Flow: m.Flow, Pressure: m.Pressure, Temp: m.Temp })
    }),
      obj = _.groupBy(array1, 'key'),
      formattedData = Object.keys(obj).reduce((acc, c) => {
        let point = _.maxBy(obj[c], 'Flow');
        //point.pressure = point.Pressure
        point.pressure = _.maxBy(obj[c], 'Pressure').Pressure; //TODO: Updated blindly, may break things
        point.flow = point.Flow;
        point.minFlow = _.minBy(obj[c], 'Flow').Flow;
        point.minPressure = _.minBy(obj[c], 'Pressure').Pressure;
        acc.push(point);
        return acc;
      }, []),
      results = formattedData.map(f => {
        var str = f.key;
        var regex = /1/gi, result, indices = [];
        while ((result = regex.exec(str))) {
          indices.push(result.index + 1);
        }
        return ({ key: indices.join(""), pressure: f.pressure.toFixed(2) - 0, flow: f.flow.toFixed(2) - 0, minFlow: f.minFlow.toFixed(2) - 0, minPressure: f.minPressure.toFixed(2) - 0 })
      })
    return results;

  }
  async webHookCtrl(request, cycleID, projectID) {
    try {
      var io = request.app.get('socketio');
      io.sockets.in(projectID).emit('refreshData', { cycleID: cycleID, projectID: projectID });
      await SegmentSettingsService.saveSegmentSettings(projectID, false);
      AlertsSender(cycleID, projectID);
      return true
    } catch (ex) {
      throw ex
    }
  }

  async loadCycles(request) {
    let date = request.body.date;
    let projectID = request.body.projectID;
    var start = new Date(date);
    let query = { projectID: projectID, "TimeStamp": { "$gte": new Date(start.getFullYear(), start.getMonth(), start.getDate()), "$lt": new Date(start.getFullYear(), start.getMonth(), start.getDate() + 1) } }
    let response = await dashboard_chart_data.aggregate([
      {
        $match: query
      }
    ])
    return response;

  }

  async avgValuesCycles(request) {
    let date = request.body.date,
      projectID = request.body.projectID,
      start = null;
    if (!date) {
      start = new Date();
      start.setDate(start.getDate() - 1);
    } else {
      start = new Date(date);
    }
    let query = { projectID: projectID, "TimeStamp": { "$gte": new Date(start.getFullYear(), start.getMonth(), start.getDate()), "$lt": new Date(start.getFullYear(), start.getMonth(), start.getDate() + 1) } }
    let response = await dashboard_chart_data.aggregate([
      {
        $match: query
      }
    ]);
    let cycles = _.groupBy(response, 'cycleID');
    let cycleDurations = Object.keys(cycles).map((key) => {
      var min = cycles[key].reduce(function (a, b) { return a.TimeStamp < b.TimeStamp ? a : b; });
      var max = cycles[key].reduce(function (a, b) { return a.TimeStamp > b.TimeStamp ? a : b; });
      return Math.floor(((max.TimeStamp - min.TimeStamp) / 1000) / 60);
    });
    let avgCycleDuration = _.mean(cycleDurations),
      formattedData = { mainData: response, numberOfCycles: Object.keys(cycles).length, avgCycleDuration: avgCycleDuration ? avgCycleDuration.toFixed(2) : 0 }
    return formattedData;

  }
  async getDataForADate(start, projectID) {
    let query = { projectID: projectID, "TimeStamp": { "$gte": new Date(start.getFullYear(), start.getMonth(), start.getDate()), "$lt": new Date(start.getFullYear(), start.getMonth(), start.getDate() + 1) } }
    let response = await dashboard_chart_data.aggregate([
      {
        $match: query
      }
    ]);
    return response;
  }
  async getDataBWDates(start, end, projectID) {
    let response = await dashboard_chart_data.aggregate([
      {
        $match: {
          $and: [{ TimeStamp: { $lte: new Date(end) } }, { TimeStamp: { $gte: new Date(start) } }],
          "projectID": projectID
        }
      }
    ]);
    return response;
  }
}

export default new DashboardService();
