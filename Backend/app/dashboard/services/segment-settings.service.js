import DashboardService from "./dashboard.service";
import MonitoringService from './monitoring.service';
import mongoose from 'mongoose';
import _ from 'lodash';
import moment from 'moment';
const SEGMENT_SETTING = mongoose.model('segment_setting');
const SEGMENT_MAPPINGS = mongoose.model('segment_mapping');
class SegmentSettingService {
    constructor() { }
    async getSegmentSettings(projectID) {
        let response = await SEGMENT_SETTING.findOne({ "projectID": projectID });
        if (!response) {
            return {};
        } else {
            let myResponse = response.toObject();
            myResponse.settings = await this.formatSegmentMappings(myResponse.settings || [], projectID)
            return myResponse;
        }


    }
    async getSegmentMappings(projectID) {
        let response = await SEGMENT_MAPPINGS.findOne({ "projectID": projectID });
        return response;

    }
    async initializeSegSettings(projectID, dateInit) {
        try {
            let recordData = await SEGMENT_SETTING.update({ projectID: projectID }, { toUpdate: true, dateInit: dateInit }, { upsert: true });
            return recordData
        } catch (ex) {
            throw ex;
        }

    }
    async saveSegmentMappings(mappings) {
        let recordData = await SEGMENT_MAPPINGS.update({ projectID: mappings.projectID }, { mappings: mappings.mappings }, { upsert: true });
        return recordData
    }
    async saveSegmentSettings(projectID, forceFully) {
        let updateRequired = false;
        let record = await SEGMENT_SETTING.findOne({ "projectID": projectID }).lean();
        if (!record)
            throw "Segment settings not inilized"
        if (forceFully) {
            updateRequired = true
        } else {

            let now = moment(new Date());
            let setDate = moment(record.dateInit);
            let difference = now.diff(setDate, 'days');
            if (difference >= 5 && record.toUpdate)
                updateRequired = true;
        }
        if (updateRequired) {
            let cycleData = await DashboardService.getDataBWDates(record.dateInit, new Date(), projectID);
            record['settings'] = this.formatSegmentSettings(cycleData);
            record['toUpdate'] = false;
            let monitoringSettings = await MonitoringService.getMonitoringSettings(projectID);
            // if (!monitoringSettings || !monitoringSettings.settings)
            await MonitoringService.saveMonitoringSettings(projectID, cycleData);
            await SEGMENT_SETTING.update({ projectID: record.projectID }, { toUpdate: record.toUpdate, settings: record.settings }, { upsert: true });
        }
        else {
            return { status: 'OK' }
        }
        return record;
    }

    async updateSegmentSettings(data) {
        let segmentSettings = await SEGMENT_SETTING.findOne({ "projectID": data.projectID }).lean();
        segmentSettings.settings.forEach(item => {
            if (item.key == data.obj.unmappedKey) {
                item.status = data.obj.status;
            }
        });
        let recordData = await SEGMENT_SETTING.update({ projectID: data.projectID }, segmentSettings, { upsert: true });
        return recordData
    }

    formatSegmentSettings(data) {
        let array1 = data.map((m) => {
            return ({ key: "" + m.M1 + m.M2 + m.M3 + m.M4 + m.M5 + m.M6, Flow: m.Flow, Pressure: m.Pressure, Temp: m.Temp })
        }),
            obj = _.groupBy(array1, 'key'),
            formattedData = Object.keys(obj).reduce((acc, c) => {
                var maxData = _.maxBy(obj[c], 'Flow');
                var maxFlow = _.maxBy(obj[c], 'Flow').Flow;
                var maxPressure = _.maxBy(obj[c], 'Pressure').Pressure;
                maxData.Flow = maxFlow;
                maxData.Pressure = maxPressure;
                acc.push(maxData);
                return acc;
            }, []),
            results = formattedData.map(f => {
                var str = f.key;
                var regex = /1/gi, result, indices = [];
                while ((result = regex.exec(str))) {
                    indices.push(result.index + 1);
                }
                return ({ key: indices.join(""), Flow: f.Flow.toFixed(2), Pressure: f.Pressure.toFixed(2), Temp: f.Temp.toFixed(2) })
            })
        return results;

    }

    async formatSegmentMappings(results, projectID) {
        let mappings = await this.getSegmentMappings(projectID);
        if (mappings) {
            mappings = mappings.toObject();
        } else {
            return results;
        }
        results = results.map(r => {
            let mapped = mappings.mappings.find(m => m.key == r.key);
            return ({ ...r, key: mapped ? mapped.mapping : r.key, unmappedKey: r.key })
        });
        results = results.filter(r => r.key);
        return results;
    }

}

export default new SegmentSettingService();
