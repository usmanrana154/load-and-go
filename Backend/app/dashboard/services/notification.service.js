import mongoose from 'mongoose'
const NOTIFICATION = mongoose.model('notification');
class NotificationService {
    constructor() { }

    async saveNotification(data) {
        let notification = await NOTIFICATION.create(data);
        return notification;
    }
    async getNotificationByCycleID(cycleID) {
        return NOTIFICATION.find({ cycleID: cycleID }).lean();
    }

}

export default new NotificationService();
