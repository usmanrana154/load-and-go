import _ from 'lodash';
import moment from 'moment';
import mongoose from 'mongoose';
const FLOW_DIVIDER = 60;
import DashboardService from './dashboard.service';
import SegmentService from './segment-settings.service';
const MONITORING_VALUES = mongoose.model('monitoring_value');
const SEGMENT_VALUES = mongoose.model('segment_setting');
class ReportService {
    constructor() { }
    async prepareReprtData(start, end, projectID) {
        let response = null;
        if (start == end)
            response = await DashboardService.getDataForADate(new Date(start), projectID);
        else
            response = await DashboardService.getDataBWDates(start, end, projectID)
        const waterConsumed = _.sumBy(response, x => x.Flow) / FLOW_DIVIDER
        let segmentSettings = await await SEGMENT_VALUES.findOne({ "projectID": projectID });
        if (segmentSettings) {
            segmentSettings = segmentSettings.toObject()
        } else {
            throw "Monitoring Values not added."
        }
        let cycles = _.groupBy(response, 'cycleID');
        let cycleDurations = Object.keys(cycles).map((key) => {
            var min = cycles[key].reduce(function (a, b) { return a.TimeStamp < b.TimeStamp ? a : b; });
            var max = cycles[key].reduce(function (a, b) { return a.TimeStamp > b.TimeStamp ? a : b; });
            return Math.floor(((max.TimeStamp - min.TimeStamp) / 1000) / 60);
        });
        let totalCycles = Object.keys(cycles).length;
        let longestCycle = _.max(cycleDurations);
        let avgCycleDuration = _.mean(cycleDurations).toFixed(2) - 0;
        let array1 = response.map((m) => {
            return ({ key: "" + m.M1 + m.M2 + m.M3 + m.M4 + m.M5 + m.M6, Flow: m.Flow, Pressure: m.Pressure, Temp: m.Temp })
        }),
            obj = _.groupBy(array1, 'key'),
            formattedData = Object.keys(obj).reduce((acc, c) => {
                let avgFlow = _.meanBy(obj[c], (p) => p.Flow).toFixed(2) - 0;
                let avgPressure = _.meanBy(obj[c], (p) => p.Pressure).toFixed(2) - 0;

                let baseLineFlow = avgFlow - 0 + 5;
                let baseLinePressure = avgPressure - 0 + 5;
                let point = { ..._.maxBy(obj[c], 'Flow'), baseLineFlow: baseLineFlow, baseLinePressure: baseLinePressure, avgFlow: avgFlow, avgPressure: avgPressure }
                acc.push(point);
                return acc;
            }, []),
            results = formattedData.map(f => {
                var str = f.key;
                var regex = /1/gi, result, indices = [];
                while ((result = regex.exec(str))) {
                    indices.push(result.index + 1);
                }
                let key = indices.join("");
                let baseObj = segmentSettings.settings.find(s => s.key == key);
                if (baseObj) {
                    f.baseLineFlow = baseObj.Flow;
                    f.baseLinePressure = baseObj.Pressure;
                }


                return ({ key: key, avgFlow: f.avgFlow, baseLineFlow: f.baseLineFlow, baseLinePressure: f.baseLinePressure, avgPressure: f.avgPressure, percentBFlow: ((f.avgFlow / f.baseLineFlow) * 100).toFixed(2), percentBPressure: ((f.avgPressure / f.baseLinePressure) * 100).toFixed(2) })
            });
        results = results.filter(r => r.key && segmentSettings.settings.find(s => s.key == r.key && s.status));
        results = await SegmentService.formatSegmentMappings(results, projectID);
        const responseData = { reportStats: { totalCycles: totalCycles, waterConsumed: waterConsumed.toFixed(2), avgCycleDuration: avgCycleDuration, longestCycle: longestCycle }, reportData: results }
        return responseData
    }
    getStandardDeviation(array) {
        const n = array.length
        const mean = array.reduce((a, b) => a + b) / n
        return Math.sqrt(array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n)
    }
    formatSegments(data, segmentMappings) {
        let array1 = data.map((m) => {
            return ({ key: "" + m.M1 + m.M2 + m.M3 + m.M4 + m.M5 + m.M6, Flow: m.Flow, Pressure: m.Pressure, Temp: m.Temp })
        }),
            obj = _.groupBy(array1, 'key'),
            formattedData = Object.keys(obj).reduce((acc, c) => {
                let avgFlow = _.meanBy(obj[c], (p) => p.Flow).toFixed(2) - 0;
                let avgPressure = _.meanBy(obj[c], (p) => p.Pressure).toFixed(2) - 0;
                let SDFlow = this.getStandardDeviation(obj[c].map(o => o.Flow)).toFixed(2) - 0;
                let SDPressure = this.getStandardDeviation(obj[c].map(o => o.Pressure)).toFixed(2) - 0;
                let point = { key: c, avgFlow: avgFlow || 0, avgPressure: avgPressure || 0, SDFlow: SDFlow || 0, SDPressure: SDPressure || 0 }
                acc.push(point);
                return acc;
            }, []),
            results = formattedData.map(f => {
                var str = f.key;
                var regex = /1/gi, result, indices = [];
                while ((result = regex.exec(str))) {
                    indices.push(result.index + 1);
                }
                let key = indices.join("");
                let status = false;
                let mapped = segmentMappings.settings.find(s => s.unmappedKey == key);
                if (mapped) {
                    key = mapped.key;
                    status = mapped.status
                }
                return ({ key: key, status: status, ['avgFlow' + key]: f.avgFlow, ['avgPressure' + key]: f.avgPressure, ['SDFlow' + key]: f.SDFlow, ['SDPressure' + key]: f.SDPressure })
            });
        results = results.filter(r => r.key && r.status);
        return results;
    }
    async prepareDetailReportData(start, end, projectID) {
        let segmentMappings = await SegmentService.getSegmentSettings(projectID);
        let response = null;
        if (start == end)
            response = await DashboardService.getDataForADate(new Date(start), projectID);
        else
            response = await DashboardService.getDataBWDates(start, end, projectID)
        let monitoringValues = await await MONITORING_VALUES.findOne({ "projectID": projectID });
        if (monitoringValues) {
            monitoringValues = monitoringValues.toObject()
        } else {
            throw "Monitoring Values not added."
        }
        let cycles = _.groupBy(response, (data) => {
            return moment(data.TimeStamp).format('MM/DD/YYYY');
        });
        let segments = this.formatSegments(response, segmentMappings);
        let formatterCycles = Object.keys(cycles).map((key) => {
            let cycleData = _.groupBy(cycles[key], 'cycleID');
            let totalCycles = Object.keys(cycleData).length;
            let cycleDurations = Object.keys(cycleData).map((key) => {
                var min = cycleData[key].reduce(function (a, b) { return a.TimeStamp < b.TimeStamp ? a : b; });
                var max = cycleData[key].reduce(function (a, b) { return a.TimeStamp > b.TimeStamp ? a : b; });
                return Math.floor(((max.TimeStamp - min.TimeStamp) / 1000) / 60);
            });
            let waterConsumed = (_.sumBy(cycles[key], x => x.Flow) / FLOW_DIVIDER).toFixed(2)
            let longestCycle = _.max(cycleDurations);
            let smallestCycle = _.min(cycleDurations);
            let avgCycleDuration = _.mean(cycleDurations).toFixed(2) - 0;

            let daySegments = Object.assign({}, ...this.formatSegments(cycles[key], segmentMappings));
            return { date: key, waterConsumed: waterConsumed, longestCycle: longestCycle, smallestCycle: smallestCycle, avgCycleDuration: avgCycleDuration, totalCycles: totalCycles, ...daySegments };
        });

        return { formatterCycles: formatterCycles, segments: segments };


    }


}

export default new ReportService();
