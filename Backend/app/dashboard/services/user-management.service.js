import mongoose from 'mongoose';
import _ from 'lodash';
import { AppSetting } from '../../../config/app.setting';
const CONFIG = AppSetting.getConfig();
const USERS = mongoose.model('user');

class UserManagementService {
    constructor() { }

    async getUsers(request) {
        const userInfo = request.userInfo;
        let response = null;
        if (userInfo.companyAdmin) {
            response = await USERS.find({ projectID: { $in: userInfo.assigned_projects }, '_id': { $ne: userInfo._id } });
        } else {
            response = await USERS.find({ superAdmin: { $exists: false } });
        }
        return await this.formatResponseArray(response)

    }
    async getUserDataByID(id) {
        let user = await USERS.findById({ _id: id });
        if (user) {
            return await this.userResponseObject(user)
        }
        return user
    }
    async createUser(data) {
        try {
            let user = new USERS();
            user.name = data.name;
            user.email = data.email.toLowerCase();
            user.projectID = data.projectID;
            user.phone = data.phone;
            user.companyAdmin = data.companyAdmin;
            user.supportUser = data.supportUser;
            user.jobTitle = data.jobTitle;
            user.notes = data.notes;
            user.address = data.address;
            user.birthday = data.birthday;
            user.notification_settings = data.notification_settings
            user.assigned_projects = data.assigned_projects;
            user.setPassword(data.password);
            let userObject = await user.save();
            let cloned = await this.userResponseObject(userObject);
            return {
                token: user.generateJwt(),
                ...cloned

            };
        } catch (e) {
            throw e;
        }

    }
    async updateUser(user, id) {
        try {
            let DBUser = await USERS.findById({ _id: id });
            DBUser.name = user.name;
            DBUser.email = user.email.toLowerCase();
            DBUser.phone = user.phone;
            DBUser.status = user.status;
            DBUser.companyAdmin = user.companyAdmin;
            DBUser.supportUser = user.supportUser;
            DBUser.jobTitle = user.jobTitle;
            DBUser.notes = user.notes;
            DBUser.address = user.address;
            DBUser.birthday = user.birthday;
            DBUser.assigned_projects = user.assigned_projects;
            DBUser.projectID = user.assigned_projects[0];
            DBUser.notification_settings = user.notification_settings
            if (user.password) {
                DBUser.setPassword(user.password)
            }
            let response = await DBUser.save();
            let cloned = await this.userResponseObject(response);
            return {
                token: DBUser.generateJwt(),
                ...cloned

            };
        } catch (e) {
            throw e
        }

    }
    async deleteUser(id) {
        try {
            let response = await USERS.findByIdAndDelete(id);
            return response;
        } catch (e) {
            throw 'Error whlie deleting the project';
        }
    }

    async uploadProfilePicture(id, image) {
        try {
            let DBUser = await USERS.findById({ _id: id });
            DBUser.selfie = CONFIG.APP.APP_ADDRESS + "/" + image;
            let response = await DBUser.save();
            let cloned = await this.userResponseObject(response);
            return {
                ...cloned

            };
        } catch (ex) {
            throw ex;
        }

    }
    async login(data) {

    }
    async userResponseObject(userObject) {
        let cloned = (await userObject).toObject();
        delete cloned.hash;
        delete cloned.salt;
        return cloned;
    }

    async getUsersByProjectID(projectID) {
        let response = await USERS.find({ projectID: {$in : [projectID]} });
        return response.map(user => user.toObject());
    }
    async formatResponseArray(array) {
        let response = [];
        for (let i = 0; i < array.length; i++) {
            let formattedUser = await this.userResponseObject(array[i]);
            response.push(formattedUser);
        }
        return response;
    }
    async getSupportUsers() {
        let response = await USERS.find({ supportUser: true });
        return await this.formatResponseArray(response)

    }

}

export default new UserManagementService();
