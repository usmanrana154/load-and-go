import mongoose from 'mongoose';
import _ from 'lodash';
import UserManagementService from './user-management.service';
const PROJECT = mongoose.model('project');
const USERS = mongoose.model('user');
import { AppSetting } from '../../../config/app.setting';
const CONFIG = AppSetting.getConfig();
class ProjectSettingService {
    constructor() { }
    async getProjectsList(request) {
        const userInfo = request.userInfo;
        let response = null;
        if (userInfo.isSuperAdmin) {
            response = await PROJECT.find({});
        } else {
            response = await PROJECT.find({ projectID: { $in: userInfo.assigned_projects } });

        }
        return response;

    }
    async getProjectDetailsByID(projectID) {
        let response = await PROJECT.findOne({ projectID: projectID });
        return response;

    }

    async saveProject(project) {
        delete project._id;
        let p = new PROJECT(project);
        try {
            let response = await p.save();
            return response;
        } catch (e) {
            throw e;
        }

    }
    async updateProject(project, id) {
        try {
            let response = await PROJECT.findOneAndUpdate({ _id: id }, project, { new: true });
            return response;
        } catch (e) {
            throw e;
        }

    }
    async getDeepdetailsProject(id) {
        let project = await this.getProjectDetailsByID(id);
        if (project) {
            project = project.toObject();
        }
        if (project.support_user) {
            project.supportUserDetails = await UserManagementService.getUserDataByID(project.support_user);

        }
        return project;
    }
    async uploadImages(id, image, type) {
        try {
            let logo = CONFIG.APP.APP_ADDRESS + "/" + image;
            let project = await PROJECT.findByIdAndUpdate(id, { [type]: logo }, { new: true })
            return project

        } catch (ex) {
            throw ex;
        }

    }


    async deleteProject(id) {
        try {
            let project = await PROJECT.findById(id);
            if (project) {
                project = project.toObject();
                let users = await USERS.find({ projectID: project.projectID });
                if (users && users.length > 0) {
                    throw 'User assigned to this project';
                } else {
                    let response = await PROJECT.findByIdAndDelete(id);
                    return response;
                }

            } else {
                throw 'Recrod not found';
            }

        } catch (e) {
            throw e;
        }
    }


}

export default new ProjectSettingService();
