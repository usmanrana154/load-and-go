import DashboardService from "./dashboard.service";
import mongoose from 'mongoose';
import _ from 'lodash';
const MONITORING_VALUES = mongoose.model('monitoring_value');
import SegmentService from './segment-settings.service';
class SegmentSettingService {
    constructor() { }
    async getMonitoringSettings(projectID) {
        let response = await MONITORING_VALUES.findOne({ "projectID": projectID });
        if (!response) {
            return {};
        } else {
            let myResponse = response.toObject();
            myResponse.settings = await SegmentService.formatSegmentMappings(myResponse.settings, projectID)
            return myResponse;
        }

    }
    async initiallizeMonitoring(projectID) {
        MONITORING_VALUES.update({ projectID: projectID }, { toUpdate: true }, { upsert: true }, function (error, recordData) {
            if (error) {
                throw 'Error saving/updating the record';
            } else {
                return recordData;
            }
        })

    }
    async saveMonitoringSettings(projectID, data) {
        let record = await MONITORING_VALUES.findOne({ "projectID": projectID });
        let recordData = {};
        if (record) {
            recordData = record.toObject();
        }
        recordData['settings'] = this.formatMonitoringSettings(data);
        recordData['toUpdate'] = false;
        await MONITORING_VALUES.update({ projectID: projectID }, { toUpdate: recordData.toUpdate, settings: recordData.settings }, { upsert: true });
        return recordData;
    }
    formatMonitoringSettings(data) {
        let array1 = data.map((m) => {
            return ({ key: "" + m.M1 + m.M2 + m.M3 + m.M4 + m.M5 + m.M6, Flow: m.Flow, Pressure: m.Pressure, Temp: m.Temp })
        }),
            obj = _.groupBy(array1, 'key'),
            formattedData = Object.keys(obj).reduce((acc, c) => {
                let point = _.maxBy(obj[c], 'Flow');
                point.minPressure = point.Pressure - 10;
                point.maxPressure = point.Pressure + 10;
                point.minFlow = point.Flow - 10;
                point.maxFlow = point.Flow + 10;
                acc.push(point);
                return acc;
            }, []),
            results = formattedData.map(f => {
                var str = f.key;
                var regex = /1/gi, result, indices = [];
                while ((result = regex.exec(str))) {
                    indices.push(result.index + 1);
                }
                return ({ key: indices.join(""), minPressure: f.minPressure.toFixed(2) - 0, minFlow: f.minFlow.toFixed(2) - 0, maxPressure: f.maxPressure.toFixed(2) - 0, maxFlow: f.maxFlow.toFixed(2) - 0 })
            })
        return results;

    }
    async updateMonitoringSettings(id, settings) {
        let response = MONITORING_VALUES.findByIdAndUpdate(id, { settings: settings }, { new: true });
        return response;
    }



}

export default new SegmentSettingService();
