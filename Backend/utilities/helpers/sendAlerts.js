import Logger from '../../config/logger';
import DashboardService from '../../app/dashboard/services/dashboard.service';
import MonitoringService from '../../app/dashboard/services/monitoring.service';
import ProjectService from '../../app/dashboard/services/project-settings.service';
import UserManagementService from '../../app/dashboard/services/user-management.service';
import NOTIFICATION_SENDER from '../services/notification.service';
import NOTIFICATION_REPOSITORY from '../../app/dashboard/services/notification.service';
import moment from 'moment'
async function sendAlertsIf(cycleID, projectID) {
    let record = await MonitoringService.getMonitoringSettings(projectID);
    if (!record || Object.keys(record).length === 0) {
        Logger.error(`No monitoring values saved for ${projectID}. Sending no alerts for ${cycleID}`);
        console.log(`No monitoring values saved for ${projectID}. Sending no alerts for ${cycleID}`);
        return;
    }
    let monitoringValues = record;
    let cycleData = await DashboardService.getData(cycleID);
    cycleData = cycleData.cycle;
    let project = await ProjectService.getProjectDetailsByID(projectID);
    if (!project) {
        Logger.error(`Project does not exist with ${projectID}, can not send alert for ${cycleID}`);
        return;
    } else {
        project = project.toObject();
    }
    let projectUsers = await UserManagementService.getUsersByProjectID(project.projectID);
    let extras = {
        cycle: cycleID,
        project: project
    }
    sendForHighFlow(monitoringValues, cycleData, projectUsers, extras);
    sendForLowFlow(monitoringValues, cycleData, projectUsers, extras);
    sendForHighPressure(monitoringValues, cycleData, projectUsers, extras);
    sendForLowPressure(monitoringValues, cycleData, projectUsers, extras);


}

function sendForHighFlow(monitoring, data, projectUsers, extras) {
    let alertData = compareWithSettings(monitoring.settings, data, 'maxFlow', 'flow', '>');
    checkAndSendAlerts(alertData, 'High Flow', projectUsers, 'flow', extras)
}
function sendForLowFlow(monitoring, data, projectUsers, extras) {
    let alertData = compareWithSettings(monitoring.settings, data, 'minFlow', 'flow', '<');
    checkAndSendAlerts(alertData, 'Low Flow', projectUsers, 'minFlow', extras)
}
function sendForHighPressure(monitoring, data, projectUsers, extras) {
    let alertData = compareWithSettings(monitoring.settings, data, 'maxPressure', 'pressure', '>');
    checkAndSendAlerts(alertData, 'High Pressure', projectUsers, 'pressure', extras)
}
function sendForLowPressure(monitoring, data, projectUsers, extras) {
    let alertData = compareWithSettings(monitoring.settings, data, 'minPressure', 'pressure', '<');
    checkAndSendAlerts(alertData, 'Low Pressure', projectUsers, 'minPressure', extras)
}
function emailDataFormatter(users, email, type) {
    const emailData = {
        [type]: {
            body: email.body,
            subject: email.subject,
            attachments: []
        },
        sendTo: users.filter(u => u.notification_settings == type || u.notification_settings == "both")
    }
    return emailData;
}
var operators = {
    '<': function (a, b) { return a < b },
    '>': function (a, b) { return a > b }
}
function compareWithSettings(monitoringSettings, cycleData, keyM, keyC, compareOperator) {
    let alertData = null,
        formatterData = DashboardService.formatSegmentsOfCycleData(cycleData);
    formatterData.some(d => {
        let faultSegment = monitoringSettings.find(s => {
            return (operators[compareOperator](d[keyC], s[keyM]) && d.key == s.unmappedKey)
        });
        if (faultSegment) {
            alertData = { segment: faultSegment, voilatedValues: d }
            Logger.info("fault segment and alert data", faultSegment, formatterData);
            return true;
        }
    });
    return alertData;
}
function getAlertType(string) {
    let alertType = '';
    switch (string) {
        case 'High Flow':
            alertType = 'high_flow';
            break;
        case 'Low Flow':
            alertType = 'low_flow';
            break;
        case 'High Pressure':
            alertType = 'high_pressure';
            break;
        case 'Low Pressure':
            alertType = 'low_pressure';
            break;
        default:
            alertType = 'default';
    }
    return alertType;
}
async function checkAndSendAlerts(alertData, message, projectUsers, key, extras) {
    if (alertData) {
        let emailBody = `${extras.project.name}, ${moment(new Date()).format('MM/DD/YYYY')}, ${extras.cycle}, ${message} (${alertData.voilatedValues[key]}) for ${alertData.segment.key}`
        let notification = {
            body: emailBody,
            subject: message
        }
        let dbRecord = {
            projectID: extras.project.projectID,
            cycleID: extras.cycle,
            message: emailBody,
            faultSegment: alertData.segment.unmappedKey,
            alertType: getAlertType(message)
        }
        await NOTIFICATION_REPOSITORY.saveNotification(dbRecord);
        const mailData = emailDataFormatter(projectUsers, notification, "email");
        await NOTIFICATION_SENDER.emailNotificationSender(mailData);
        const smsData = emailDataFormatter(projectUsers, notification, "sms")
        await NOTIFICATION_SENDER.smsNotificationSender(smsData);
    }
}





module.exports = sendAlertsIf;