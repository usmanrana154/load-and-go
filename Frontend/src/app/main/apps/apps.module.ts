import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [

    {
        path: 'dashboards/project',
        loadChildren: './dashboards/project/project.module#ProjectDashboardModule'
    },
    {
        path: 'settings/segment-settings',
        loadChildren: './settings/segment-settings/segment.module#SegmentSettingsModule'
    },
    {
        path: 'settings/project-settings',
        loadChildren: './settings/project-settings/contacts.module#ContactsModule'
    },
    {
        path: 'contacts',
        loadChildren: './contacts/contacts.module#ContactsModule'
    },
    {
        path: 'dashboards/report',
        loadChildren: './dashboards/report/report.module#ReportDashboardModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class AppsModule {
}
