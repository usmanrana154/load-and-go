import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { fuseAnimations } from '@fuse/animations';
import { ReportDashboardService } from 'app/main/apps/dashboards/report/report.service';
import { GlobalDataService } from '@fuse/services/global.data.service';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
    selector: 'report-dashboard',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ReportDashboardComponent implements OnInit {
    rows: any[];
    loadingIndicator: boolean;
    reorderable: boolean;
    reportStats: any;
    reportDates: any = {};
    userData: any = {};
    columnsConfigs: any = {};
    detailReportRows: any;
    detailReportSegments: any;
    columns = ['Cycle Segment', 'Average', 'Baseline', '% Baseline']


    private _unsubscribeAll: Subject<any>;

    constructor(
        private _projectDashboardService: ReportDashboardService,
        private _globalDataService: GlobalDataService,
        private _httpClient: HttpClient

    ) {
        this.columnsConfigs = {
            pressure: [{ name: this.columns[0], prop: "key" }, { name: this.columns[1], prop: "avgPressure" },
            { name: this.columns[2], prop: "baseLinePressure" }, { name: this.columns[3], prop: "percentBPressure" }],
            flow: [{ name: this.columns[0], prop: "key" }, { name: this.columns[1], prop: "avgFlow" },
            { name: this.columns[2], prop: "baseLineFlow" }, { name: this.columns[3], prop: "percentBFlow" }]
        }


    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.userData = this._globalDataService.getUserData();
    }

    exportCsv = () => {
        let dataStats = this._globalDataService.ConvertToCSV('', [this.reportStats], [{ name: 'Average Cycle Time(Minutes)', prop: "avgCycleDuration" }, { name: 'Longest Cycle(Minutes)', prop: "longestCycle" }, { name: 'Total Cycles Run', prop: "totalCycles" }, { name: 'Total Water Consumed(Gallons)', prop: "waterConsumed" }]);
        let dataPressure = this._globalDataService.ConvertToCSV('Pressure Summary(PSIG)', this.rows, [...this.columnsConfigs.pressure]);
        let dataFlow = this._globalDataService.ConvertToCSV('Flow Summary(GPM)', this.rows, [...this.columnsConfigs.flow]);
        this._globalDataService.downloadCsv(dataStats + dataFlow + dataPressure, `report_data_${new Date()}`)
    }
    exportDetailedCsv = () => {
        let segments = this.detailReportSegments;
        let formattedColumns = segments.map(s => {
            return ([{ name: 'Segment ' + s.key, prop: 'nothing' },
            { name: 'Average Flow', prop: "avgFlow" + s.key },
            { name: 'Flow Standard Deviation', prop: "SDFlow" + s.key },
            { name: 'Average PSI', prop: "avgPressure" + s.key },
            { name: 'PSI Standard Deviation', prop: "SDPressure" + s.key },

            ])
        });
        let columnss = _.flatten(formattedColumns);
        let columns = [{ name: 'Date', prop: "date" },
        { name: '# of Cycles', prop: "totalCycles" },
        { name: 'Average Cycle Time', prop: "avgCycleDuration" },
        { name: 'Shortest Cycle Time', prop: "smallestCycle" },
        { name: 'Longest Cycle Time', prop: "longestCycle" },
        { name: 'Approximate Water Used', prop: "waterConsumed" },
        ...columnss

        ]
        let dataStats = this._globalDataService.ConvertToCSV('', this.detailReportRows, columns);
        this._globalDataService.downloadCsv(dataStats, `report_data_${new Date()}`)

    }

    getReportData(from, to) {
        if (!from || !to) {
            return
        }
        this._httpClient.post((<any>window).appBaseUrl + '/api/reports/getReport', { startDate: from, endDate: to, projectID: this.userData.projectID || "3541" })
            .subscribe((response: any) => {
                this.rows = response.reportData;
                this.reportStats = response.reportStats;
                this.loadingIndicator = false;
            });
    }
    getDetailReportData(from, to) {
        if (!from || !to) {
            return
        }
        this._httpClient.post((<any>window).appBaseUrl + '/api/reports/getDetailReport', { startDate: from, endDate: to, projectID: this.userData.projectID || "3541" })
            .subscribe((response: any) => {

                this.detailReportRows = response.formatterCycles;
                this.detailReportSegments = response.segments;
                this.exportDetailedCsv();
            });
    }
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions

    }
}



