import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { ReportDashboardComponent } from 'app/main/apps/dashboards/report/report.component';
import { ReportDashboardService } from 'app/main/apps/dashboards/report/report.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';





const routes: Routes = [
    {
        path: '**',
        component: ReportDashboardComponent,
        resolve: {
            data: ReportDashboardService
        }
    }
];

@NgModule({
    declarations: [
        ReportDashboardComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatSelectModule,
        MatTableModule,
        MatInputModule,
        MatDatepickerModule,
        NgxDatatableModule,
        FormsModule,
        CommonModule,
        MatDividerModule,
        FlexLayoutModule
    ],
    providers: [
        ReportDashboardService
    ]
})
export class ReportDashboardModule {
}

