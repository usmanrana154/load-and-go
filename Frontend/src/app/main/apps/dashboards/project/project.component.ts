import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable } from 'rxjs';
import * as shape from 'd3-shape';
import * as _ from 'lodash';
import { fuseAnimations } from '@fuse/animations';
import * as Highcharts from "highcharts";
import GanttModule from "highcharts/modules/gantt";
import { ProjectDashboardService } from 'app/main/apps/dashboards/project/project.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { GlobalDataService } from '@fuse/services/global.data.service';
import * as moment from 'moment';
import { Socket } from 'ngx-socket-io';
GanttModule(Highcharts);
@Component({
    selector: 'project-dashboard',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProjectDashboardComponent implements OnInit {
    projects: any[];
    selectedProject: any;
    dashBoardData: any = {};
    widgets: any;
    widget5: any = {};
    widget6: any = {};
    widget7: any = {};
    widget8: any = {};
    widget9: any = {};
    widget11: any = {};
    widgetFlow: any = {};
    widgetPressure: any = {};
    ganttChartData: any = [];
    manifold_word: string = "Manifold";
    ghantChatReady: any = {};
    manifoldsArray: any = ["M1", "M2", "M3", "M4", "M5", "M6"];
    updateFromInput: boolean = false; //For gantt chart specifically
    maxDateStatWidget: any;
    userData: any;
    projectDetails: any = {};
    pipelineChartOptions: any = {
        chart: {
            height: 300
        },
        credits: {
            enabled: false
        },
        title: {
            text: ""
        },
        xAxis: [{
            currentDateIndicator: true,
        }, {
            opposite: false,
            visible: false
        }],
        yAxis: {
            uniqueNames: true,
            labels: {
                useHTML: true,
                style: {
                    width: "150px",
                    height: "25px",
                    fontSize: "10px"
                }
            }
        },
        series: [
            {
                name: "Manifold",
                type: "gantt",
                data: [

                ]
            }
        ]
    };
    Highcharts: typeof Highcharts = Highcharts;

    optFromInput: Highcharts.Options = this.pipelineChartOptions;


    dateNow = Date.now();

    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {ProjectDashboardService} _projectDashboardService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private _projectDashboardService: ProjectDashboardService,
        private socket: Socket,
        private _globalDataService: GlobalDataService
    ) {
        let date = new Date();
        this.dashBoardData.cycleTimeStamp = new Date(new Date().setHours(0, 0, 0, 0));
        date.setDate(date.getDate() - 1);
        this.maxDateStatWidget = date;
        this.dashBoardData.avgCycleCals = this.maxDateStatWidget;
        this.Highcharts.setOptions({
            time: {
                useUTC: false
            }
        });

        /**
         * Widget Flow
         */
        this.widgetFlow = {
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showGridLines: false,
            showXAxisLabel: true,
            xAxisLabel: 'TIME',
            axisFormat(val) {
                if (this.ticks[0] == val || this.ticks[this.ticks.length - 1] == val || this.ticks[(this.ticks.length - 1) / 2] == val) {
                    return val
                } else {
                    return ''
                }
            },
            showYAxisLabel: true,
            yAxisLabel: 'FLOW(GPM)',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: '',
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: 'TIME',
                showYAxisLabel: false,
                yAxisLabel: 'FLOW(GPM)',
                scheme: {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                curve: shape.curveStepBefore
            }
        };
        /**
         * Pressure Gadget
         */
        this.widgetPressure = {
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showGridLines: true,
            showXAxisLabel: true,
            xAxisLabel: 'TIME',
            autoScale: true,
            showYAxisLabel: true,
            axisFormat(val) {
                if (this.ticks[0] == val || this.ticks[this.ticks.length - 1] == val || this.ticks[(this.ticks.length - 1) / 2] == val) {
                    return val
                } else {
                    return ''
                }
            },
            yAxisLabel: 'PRESSURE(PSIG)',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: '',
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: 'TIME',
                showYAxisLabel: true,
                yAxisLabel: 'PRESSURE(PSIG)',
                scheme: {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                curve: shape.curveBasis
            }
        };
        /**
         * Widget 5
         */
        this.widget5 = {
            currentRange: 'TW',
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: '',
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: 'Days',
                showYAxisLabel: false,
                yAxisLabel: 'Isues',
                scheme: {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                curve: shape.curveStepBefore
            }
        };

        /**
         * Widget 6
         */
        this.widget6 = {
            currentRange: 'TW',
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: true,
            gradient: false,
            scheme: {
                domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        /**
         * Widget 7
         */
        this.widget7 = {
            currentRange: 'T'
        };

        /**
         * Widget 8
         */
        this.widget8 = {
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: false,
            gradient: false,
            scheme: {
                domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63', '#ffc107']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        /**
         * Widget 9
         */
        this.widget9 = {
            currentRange: 'TW',
            xAxis: false,
            yAxis: false,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            curve: shape.curveBasis
        };

        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    async ngOnInit(): Promise<any> {
        this.userData = this._globalDataService.getUserData();
        this.dashBoardData.getOrignalImage = this._globalDataService.getOrignalImage;
        this.projectDetails = this._globalDataService.getGlobalData().projectDetails;
        this.projects = this._projectDashboardService.projects;
        this.selectedProject = this.projects[0];
        this.widgets = this._projectDashboardService.widgets;
        this.dashBoardData.plotsData = this._projectDashboardService.dashBoardData;
        this.dashBoardData.segmentMonitoringSettings = this._projectDashboardService.monitoring_setting;
        this.dashBoardData.staticWidgets = this._projectDashboardService.staticWidgets;
        await this.loadWeatherData(this.userData.zipCode);
        this.loadCycles(true);
        this.widget11.onContactsChanged = new BehaviorSubject({});
        this.widget11.onContactsChanged.next(this.widgets.widget11.table.rows);
        this.widget11.dataSource = new FilesDataSource(this.widget11);
        this.socket.emit('createRoom', this.userData.projectID || '3541');
        this.socket.on('refreshData', (data) => {
            console.log("refreshData");
            if (this._globalDataService.getGlobalData().refreshDashboard)
                this.refreshDate(data.cycleID);
        });

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    refreshDate(cycleID): void {
        if (cycleID == 'none' || !cycleID) {
            return;
        }
        this._projectDashboardService.getDashboardData(cycleID)
            .then((response) => {
                this.dashBoardData.plotsData = response.cycle || [];
                this.dashBoardData.cycleAlerts = response.alerts || [];
                this.dashBoardData.segmentSettings = response.segments || [];
                this.formatData();


            });
    }
    async loadWeatherData(zipCode) {
        if (!zipCode)
            return
        this.dashBoardData.weatherWidget = await this._projectDashboardService.getWeatherWidgetData(zipCode)

    }
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
    reLoadStaticWidgets(data): void {
        this._projectDashboardService.getStaticWidgetsData(data, this.userData.projectID)
            .then((cycles) => {
                this.dashBoardData.staticWidgets = cycles;


            });
    }

    formatData = () => {
        this.formatPressure();
        this.formatFlow();
        this.formatManiFlod();
        this.formatCycleDetailsInWeather();
    }

    formatPressure(): void {

        let data = this.dashBoardData.plotsData.map(function (d) {
            return { time: d.TimeStamp, value: d.Pressure }
        });
        data.sort(function compare(a, b) {
            var dateA: any = new Date(a.time);
            var dateB: any = new Date(b.time);
            return dateA - dateB;
        });
        this.widgetPressure.dataset = [{ name: "Pressure", series: data.map(z => ({ name: moment(z.time).format('HH:mm:ss'), "value": z.value })) }];
        this.widgetFlow.curve = shape.curveBasis;
        this.widgetPressure.ready = true;
    }
    formatFlow(): void {
        let data = this.dashBoardData.plotsData.map(function (d) {
            return { time: d.TimeStamp, value: d.Flow }
        });
        data.sort(function compare(a, b) {
            var dateA: any = new Date(a.time);
            var dateB: any = new Date(b.time);
            return dateA - dateB;
        });
        this.widgetFlow.dataset = [{ name: "Flow", series: data.map(z => ({ name: moment(z.time).format('HH:mm:ss'), "value": z.value })) }];
        this.widgetFlow.curve = shape.curveStepBefore;
        this.widgetFlow.ready = true;
    }
    formatManiFlod(): void {
        this.ghantChatReady = false;
        let result = [],
            flag = false;
        this.dashBoardData.plotsData = _.orderBy(this.dashBoardData.plotsData, (o: any) => {
            return moment(o.TimeStamp);
        }, ['asc']);
        this.manifoldsArray.map((manifold) => {
            for (let i = 0; i < this.dashBoardData.plotsData.length; i++) {
                if (this.dashBoardData.plotsData[i][manifold] == 1 && !flag) {
                    result.push({ color: '#6ab04c', name: manifold, start: new Date(this.dashBoardData.plotsData[i].TimeStamp).getTime() });
                    flag = true;
                }
                if (flag && this.dashBoardData.plotsData[i][manifold] == 0) {
                    result[result.length - 1].end = new Date(this.dashBoardData.plotsData[i].TimeStamp).getTime();
                    flag = false;

                }
                if (flag && this.dashBoardData.plotsData[i][manifold] == 1 && i == (this.dashBoardData.plotsData.length - 1)) {
                    result[result.length - 1].end = new Date(this.dashBoardData.plotsData[i].TimeStamp).getTime();
                    flag = false;
                }
            }
        })
        let formattedResults: any = _.groupBy(result, 'name');
        for (let i = 0; i < this.manifoldsArray.length; i++) {
            if (!formattedResults[this.manifoldsArray[i]]) {
                formattedResults[this.manifoldsArray[i]] = [{ name: this.manifoldsArray[i], start: undefined, end: undefined, y: i }];
            } else {
                formattedResults[this.manifoldsArray[i]][0].y = i;
            }
        }
        var data = [
            ...formattedResults.M1,
            ...formattedResults.M2,
            ...formattedResults.M3,
            ...formattedResults.M4,
            ...formattedResults.M5,
            ...formattedResults.M6

        ];

        for (let i = 0; i < data.length; i++) {
            let point = data[i];
            let flag = this.dashBoardData.cycleAlerts.some(alert => {
                return alert.faultSegment.indexOf(point.name[1]) > -1
            })
            if (flag)
                point.color = '#F44336';
        }
        setTimeout(() => {
            this.pipelineChartOptions.series[0].data = data;
            this.optFromInput = this.pipelineChartOptions;
            this.ghantChatReady = true;
            this.updateFromInput = true;
        }, 0)
    }
    formatCycleDetailsInWeather() {
        if (this.dashBoardData.plotsData.length > 0) {
            this.dashBoardData.weatherWidget['cycleDetails'] = { temperature: _.meanBy(this.dashBoardData.plotsData, 'Temp').toFixed(1), timeStamp: this.dashBoardData.plotsData[0].TimeStamp };
            this.dashBoardData.weatherWidget['cycleAlerts'] = this.dashBoardData.cycleAlerts;
        }

    }

    getDateDifference(from, to) {
        let a: any = new Date(from);
        let b: any = new Date(to);
        let difference = (b - a) / 1000;
        return difference;

    }
    dateFormatter(date) {
        return moment(date).format('MM/DD/YYYY');
    }
    loadCycles(first_load): void {
        this._projectDashboardService.loadCycles(this.dashBoardData.cycleTimeStamp, this.userData.projectID)
            .then((cycles) => {
                cycles.sort(function compare(a, b) {
                    var dateA: any = new Date(a.TimeStamp);
                    var dateB: any = new Date(b.TimeStamp);
                    return dateA - dateB;
                });
                let formatData = _.groupBy(cycles, "cycleID");

                this.dashBoardData.loadedCycles = formatData;
                if (first_load && Object.keys(formatData).length > 0) {
                    let cycle = Object.keys(formatData)[0]
                    this.refreshDate(cycle)
                    this.dashBoardData.selectedCycle = cycle;
                } else
                    this.dashBoardData.selectedCycle = 'none';

            });
    }
    selectProjectForSA(project) {
        let user = this._globalDataService.getUserData();
        this._projectDashboardService.getprojectDeepDetails(project.projectID).then((projectDetails) => {
            user.projectID = projectDetails.projectID;
            user.zipCode = project.zipCode;
            user.projectDetails = project;
            this._globalDataService.setUserData(user);
            this._globalDataService.setGlobalData({ projectDetails: projectDetails })
            window.location.reload();
        })


    }
}

export class FilesDataSource extends DataSource<any>
{
    /**
     * Constructor
     *
     * @param _widget11
     */
    constructor(private _widget11) {
        super();
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        return this._widget11.onContactsChanged;
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

