import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GlobalDataService } from '@fuse/services/global.data.service';

import { Observable } from 'rxjs';

@Injectable()
export class ProjectDashboardService implements Resolve<any>
{
    projects: any[];
    widgets: any[];
    dashBoardData: any[];
    staticWidgets: any = {};
    monitoring_setting: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _globalDataService: GlobalDataService
    ) {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        let promises = [
            this.getProjects(),
            this.getWidgets(),
            this.getDashboardDataByprojectID(),
            this.getStaticWidgetsData(null, this._globalDataService.getUserData().projectID),
            this.getMonitoringSettings()
        ];


        return new Promise((resolve, reject) => {

            Promise.all(
                promises
            ).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get projects
     *
     * @returns {Promise<any>}
     */
    getProjects(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/settings/projects')
                .subscribe((response: any) => {
                    this.projects = response;
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get widgets
     *
     * @returns {Promise<any>}
     */
    getWidgets(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/project-dashboard-widgets')
                .subscribe((response: any) => {
                    this.widgets = response;
                    resolve(response);
                }, reject);
        });
    }
    getDashboardDataByprojectID(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/dashboard/data/byProjectID/' + this._globalDataService.getUserData().projectID)
                .subscribe((response: any) => {
                    this.dashBoardData = response;

                    resolve(response);
                }, reject);
        });
    }

    getDashboardData(cycleID?: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/dashboard/data/byCycleID/' + cycleID)
                .subscribe((response: any) => {
                    this.dashBoardData = response;

                    resolve(response);
                }, reject);
        });
    }

    getprojectDeepDetails(projectID): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/settings/projectDetails/' + projectID)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    getMonitoringSettings(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/monitoring/' + (this._globalDataService.getUserData().projectID || 3541))
                .subscribe((response: any) => {
                    if (response)
                        this.monitoring_setting = response;
                    else {
                        this.monitoring_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }

    loadCycles(date, projectID): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post((<any>window).appBaseUrl + '/api/dashboard/data/loadCycles', { date: date, projectID: projectID })
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
    getStaticWidgetsData(date, projectID): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post((<any>window).appBaseUrl + '/api/dashboard/data/avgValuesCycles', { date: date, projectID: projectID })
                .subscribe((response: any) => {
                    this.staticWidgets = response;
                    resolve(response);
                }, reject);
        });
    }

    getWeatherWidgetData(zipCode): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(`http://api.weatherapi.com/v1/current.json?key=8e8f5164180f4e028de22611200507&q=${zipCode}`)
                .subscribe((response: any) => {
                    this.dashBoardData = response;

                    resolve(response);
                }, reject);
        });
    }


}
