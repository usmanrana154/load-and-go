import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { AnalyticsDashboardService } from 'app/main/apps/dashboards/analytics/analytics.service';
import * as moment from 'moment';
import * as shape from 'd3-shape';
@Component({
    selector: 'analytics-dashboard',
    templateUrl: './analytics.component.html',
    styleUrls: ['./analytics.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AnalyticsDashboardComponent implements OnInit {
    widgets: any;
    widget1SelectedYear = '2016';
    widget5SelectedDay = 'today';
    dashBoardData: any;
    widget5: any = {};

    /**
     * Constructor
     *
     * @param {AnalyticsDashboardService} _analyticsDashboardService
     */
    constructor(
        private _analyticsDashboardService: AnalyticsDashboardService
    ) {
        // Register the custom chart.js plugin
        this._registerCustomChartJSPlugin();
       
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Get the widgets from the service
        this.widgets = this._analyticsDashboardService.widgets;
        this.widgets.widget1.curve = shape.curveStepBefore;
        console.log(this.widgets)
        //this.dashBoardData = this._analyticsDashboardService.dashBoardData;
        //this.formatData();
        

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register a custom plugin
     */
    private _registerCustomChartJSPlugin(): void {
        (window as any).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            //const dataString = dataset.data[index].toString() + 'GPM';
                            const dataString = dataset.data[index].toString()
                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }
    formatData(): void {
        this.formatPressure();
    }

    formatPressure(): void {
        let data = this.dashBoardData.slice(0, 19).map(function (d) {
            return { time: d.TimeStamp, value: d.Pressure }
        });
        this.dashBoardData['pressureWidget'] = {};
        this.dashBoardData.pressureWidget.dataset = [{ label: "Pressure", data: data.map(z => z.value) }];
        this.dashBoardData.pressureWidget.labels = data.map(z => moment(z.time).format('HH:mm:ss'));
        this.widgets.widget1.options.curve = shape.curveCatmullRomOpen;
        this.dashBoardData.pressureWidget.ready = true;
    }
    formatFlow(): void {
        let data = this.dashBoardData.slice(0, 19).map(function (d) {
            return { time: d.TimeStamp, value: d.Flow }
        });
        this.dashBoardData['FlowWidget'] = {};
        this.dashBoardData.FlowWidget.dataset = [{ label: "Flow", data: data.map(z => z.value) }];
        this.dashBoardData.FlowWidget.labels = data.map(z => moment(z.time).format('HH:mm:ss'));
        this.dashBoardData.FlowWidget.curve = shape.curveStepBefore;
        this.dashBoardData.FlowWidget.ready = true;


    }

}

