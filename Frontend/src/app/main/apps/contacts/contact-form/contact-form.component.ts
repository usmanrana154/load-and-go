import { Component, EventEmitter, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { ContactsService } from 'app/main/apps/contacts/contacts.service';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';

@Component({
    selector: 'contacts-contact-form-dialog',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactsContactFormDialogComponent {
    action: string;
    contact: any = {};
    contactForm: FormGroup;
    dialogTitle: string;
    projects: any[];
    public uploader: FileUploader = new FileUploader({ autoUpload: true, itemAlias: 'file', headers: [{ name: 'Authorization', value: 'Token ' + JSON.parse(localStorage.getItem('user')).token }] });



    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ContactsContactFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _contactService: ContactsService
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit User';
            this.contact = _data.contact;
        }
        else {
            this.dialogTitle = 'New User';
            this.contact = {};
        }

        this.contactForm = this.createContactForm();

    }
    ngOnInit(): void {
        this.projects = this._contactService.projects;
        this.uploader.setOptions({ url: (<any>window).appBaseUrl + '/api/uploadProfile/' + this.contact._id })
        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            this.contact.selfie = JSON.parse(response).selfie;
            let index = this._contactService.contacts.findIndex(c => c._id == this.contact._id);
            this._contactService.contacts[index] = this.contact;
            this._contactService.onContactsChanged.next(this._contactService.contacts);

        };


    }
    public onFileSelected(event: EventEmitter<File[]>) {
        this.uploader.uploadAll();

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    createContactForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.contact._id],
            name: [this.contact.name],
            password: [this.contact.password],
            selfie: [this.contact.selfie],
            assigned_projects: [this.contact.assigned_projects],
            jobTitle: [this.contact.jobTitle],
            email: [this.contact.email],
            phone: [this.contact.phone],
            address: [this.contact.address],
            birthday: [this.contact.birthday],
            notification_settings: [this.contact.notification_settings],
            notes: [this.contact.notes]
        });
    }
}
