import { Component, EventEmitter, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'alerts-form-dialog',
    templateUrl: './alerts-form.component.html',
    styleUrls: ['./alerts-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactsContactFormDialogComponent {
    monitoringValues: any;

    constructor(
        public matDialogRef: MatDialogRef<ContactsContactFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
    ) {
        this.monitoringValues = _data.monitoringValues
    }
    ngOnInit(): void {



    }



}
