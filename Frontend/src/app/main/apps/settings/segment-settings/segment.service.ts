import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { GlobalDataService } from '@fuse/services/global.data.service';

@Injectable()
export class SegmentSettingsService implements Resolve<any>
{
    segment_setting: any;
    segment_mappings: any;
    monitoring_setting: any


    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _globalDataService: GlobalDataService
    ) {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getSegmentSettings(),
                this.getMonitoringSettings(),
                this.getSegmentMappings()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get projects
     *
     * @returns {Promise<any>}
     */
    getSegmentSettings(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/settings/segment-settings/' + (this._globalDataService.getUserData().projectID || 3541))
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }
    getSegmentMappings(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/settings/segment-mappings/' + (this._globalDataService.getUserData().projectID || 3541))
                .subscribe((response: any) => {
                    if (response)
                        this.segment_mappings = response;
                    else {
                        this.segment_mappings = { mappings: [] };
                    }
                    resolve(response);
                }, reject);
        });
    }
    saveSegmentMappings(data): Promise<any> {
        return new Promise((resolve, reject) => {

            this._httpClient.post((<any>window).appBaseUrl + '/api/settings/segment-mappings', { mappings: data.mappings, projectID: this._globalDataService.getUserData().projectID })
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }
    updateSegmentSettings(obj): Promise<any> {
        return new Promise((resolve, reject) => {

            this._httpClient.put((<any>window).appBaseUrl + '/api/settings/segment-settings', { obj: obj, projectID: this._globalDataService.getUserData().projectID })
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }
    saveSegmentSettingsForceFully(data): Promise<any> {
        return new Promise((resolve, reject) => {

            this._httpClient.post((<any>window).appBaseUrl + '/api/settings/segment-settings', { ...data })
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }
    
    getMonitoringSettings(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get((<any>window).appBaseUrl + '/api/monitoring/' + (this._globalDataService.getUserData().projectID || 3541))
                .subscribe((response: any) => {
                    if (response)
                        this.monitoring_setting = response;
                    else {
                        this.monitoring_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }
    saveSegmentSettings(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post((<any>window).appBaseUrl + '/api/settings/initialize-segment-settings', { ...data })
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }

    saveMonitoringSettings(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post((<any>window).appBaseUrl + '/api/monitoring/initialize', { ...data })
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }

    updateMonitoringSettings(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.put((<any>window).appBaseUrl + '/api/monitoring', { ...data })
                .subscribe((response: any) => {
                    if (response)
                        this.segment_setting = response;
                    else {
                        this.segment_setting = {};
                    }
                    resolve(response);
                }, reject);
        });
    }



}
