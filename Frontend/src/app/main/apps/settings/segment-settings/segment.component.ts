import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SegmentSettingsService } from 'app/main/apps/settings/segment-settings/segment.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ContactsContactFormDialogComponent } from 'app/main/apps/settings/segment-settings/alerts-form/alerts-form.component';
import { GlobalDataService } from '@fuse/services/global.data.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
@Component({
    selector: 'segment-settings',
    templateUrl: './segment.component.html',
    styleUrls: ['./segment.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class SegmentSettingsComponent {
    /**
     * Constructor
     */
    //@ViewChild('dialogContent', { static: false })
    setting: any = {};
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    dialogRef: any;
    constructor(
        private _segmentSettingsService: SegmentSettingsService,
        public _matDialog: MatDialog,
        private _globalDataService: GlobalDataService,

    ) {

    }
    editMonitoringValues(): void {
        this.dialogRef = this._matDialog.open(ContactsContactFormDialogComponent, {
            panelClass: 'contact-form-dialog',
            data: {
                monitoringValues: this.setting.monitoring_setting
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: any) => {
                if (response[0] == 'close') {
                    return;
                }
                let data = response[1];
                this._segmentSettingsService.updateMonitoringSettings(data).then(res => {
                })


            });
    }
    ngOnInit(): void {
        this.setting.segment_settings = this._segmentSettingsService.segment_setting;
        this.setting.monitoring_setting = this._segmentSettingsService.monitoring_setting;
        this.setting.segment_mappings = this._segmentSettingsService.segment_mappings;
    }
    initiateLearning(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to start learning?';
        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._segmentSettingsService.saveSegmentSettings({ projectID: this._globalDataService.getUserData().projectID })
                    .then((cycles) => {
                        this.setting.segment_settings.toUpdate = true;
                    });

            }
            this.confirmDialogRef = null;
        });

    }
    initiateMonitoring(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to start monitoring?';
        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._segmentSettingsService.saveSegmentSettingsForceFully({ forceFully: true, projectID: this._globalDataService.getUserData().projectID })
                    .then((cycles) => {
                        this.setting.segment_settings.toUpdate = false;
                    });

            }
            this.confirmDialogRef = null;
        });

    }
    saveSegmentMappings() {
        this._segmentSettingsService.saveSegmentMappings(this.setting.segment_mappings).then(response => {
            console.log(response);
        })
    }

    updateSegmentSettings(row) {
        this._segmentSettingsService.updateSegmentSettings(row).then(response => {
            console.log(response);
        })
    }
    addToMappingArray = () => {
        this.setting.segment_mappings.mappings.push({ key: "", mapping: "" })
    }
    openAlertsFormDialog() {

    }

}
