import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HighchartsChartModule } from 'highcharts-angular';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseConfirmDialogModule } from '@fuse/components';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { MatDialogModule } from '@angular/material/dialog';

import { SegmentSettingsComponent } from 'app/main/apps/settings/segment-settings/segment.component';
import { SegmentSettingsService } from 'app/main/apps/settings/segment-settings/segment.service';
import { ContactsContactFormDialogComponent } from 'app/main/apps/settings/segment-settings/alerts-form/alerts-form.component';
const config: SocketIoConfig = { url: (<any>window).appBaseUrl, options: {} };
const routes: Routes = [
    {
        path: '**',
        component: SegmentSettingsComponent,
        resolve: {
            data: SegmentSettingsService
        }
    }
];

@NgModule({
    declarations: [
        SegmentSettingsComponent,
        ContactsContactFormDialogComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        MatDialogModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        MatInputModule,
        MatDatepickerModule,
        NgxChartsModule,
        HighchartsChartModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        FuseConfirmDialogModule,
        SocketIoModule.forRoot(config),
        MatSlideToggleModule
    ],
    providers: [
        SegmentSettingsService
    ],
    entryComponents: [
        ContactsContactFormDialogComponent
    ]
})
export class SegmentSettingsModule {
}

