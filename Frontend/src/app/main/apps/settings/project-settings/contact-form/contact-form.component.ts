import { Component, Inject, EventEmitter, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ContactsService } from 'app/main/apps/settings/project-settings/contacts.service';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';


@Component({
    selector: 'contacts-contact-form-dialog',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ContactsContactFormDialogComponent {
    action: string;
    contact: any = {};
    contactForm: FormGroup;
    dialogTitle: string;
    users: any;
    fileType: any;
    public uploader: FileUploader = new FileUploader({ autoUpload: true, itemAlias: 'file', headers: [{ name: 'Authorization', value: 'Token ' + JSON.parse(localStorage.getItem('user')).token }] });


    /**
     * Constructor
     *
     * @param {MatDialogRef<ContactsContactFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ContactsContactFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _projectService: ContactsService
    ) {
        // Set the defaults
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Project';
            this.contact = _data.contact;
        }
        else {
            this.dialogTitle = 'New Project';
            this.contact = {};
        }

        this.contactForm = this.createContactForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create contact form
     *
     * @returns {FormGroup}
     */
    ngOnInit(): void {
        this.users = this._projectService.supportUsers;
        this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
            form.append('type', this.fileType);
        };
        this.uploader.setOptions({ url: (<any>window).appBaseUrl + '/api/settings/projects/uploadImages/' + this.contact._id })
        this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            this.contact.selfie = JSON.parse(response).selfie;
            let index = this._projectService.projects.findIndex(c => c._id == this.contact._id);
            this._projectService.projects[index] = this.contact;
            this._projectService.onContactsChanged.next(this._projectService.projects);

        };

    }
    public onFileSelected(event: EventEmitter<File[]>) {
        this.uploader.uploadAll();

    }
    createContactForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.contact._id],
            projectID: [this.contact.projectID],
            name: [this.contact.name],
            pressureSensor: [this.contact.pressureSensor],
            flowMeter: [this.contact.flowMeter],
            optoG: [this.contact.optoG],
            notes: [this.contact.notes],
            solenoidValues: [this.contact.solenoidValues],
            support_user: [this.contact.support_user],
            zipCode: [this.contact.zipCode || '', [Validators.required, Validators.maxLength(5)]]
        });
    }
}
